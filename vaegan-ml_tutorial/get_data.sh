#!/usr/bin/env bash

mkdir -p datasets/Calo
cd datasets/Calo

for FILE in MiddleOnly_CellEnergies.npy MiddleOnly_CellEta.npy MiddleOnly_CellPhi.npy TrueEnergy.npy; do
    wget http://dguest-public.web.cern.ch/dguest-public/tutorials/data/2019/workshop/caloae/Calo/${FILE}
done
